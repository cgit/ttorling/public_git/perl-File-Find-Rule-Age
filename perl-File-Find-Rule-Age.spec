Name:           perl-File-Find-Rule-Age
Version:        0.302
Release:        3%{?dist}
Summary:        Rule to match on file age
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-Find-Rule-Age/
Source0:        http://www.cpan.org/authors/id/R/RE/REHSACK/File-Find-Rule-Age-%{version}.tar.gz
BuildArch:      noarch
# Build:
BuildRequires:  make
BuildRequires:  findutils
BuildRequires:  coreutils
BuildRequires:  perl
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Run-time:
BuildRequires:  perl(DateTime) >= 0.42
BuildRequires:  perl(File::Find::Rule) >= 0.30
BuildRequires:  perl(File::stat)
BuildRequires:  perl(Params::Util) >= 0.37
# Tests:
BuildRequires:  perl(base)
BuildRequires:  perl(Carp)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(File::Temp)
BuildRequires:  perl(File::Touch)
BuildRequires:  perl(FindBin)
BuildRequires:  perl(Test::More) >= 0.9
Requires:       perl(DateTime) >= 0.42
Requires:       perl(File::Find::Rule) >= 0.30
Requires:       perl(Params::Util) >= 0.37
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%global __requires_exclude ^perl\\(DateTime|File::Find::Rule|Params::Util\\)$

%description
File::Find::Rule::Age makes it easy to search for files based on their age.
DateTime and File::stat are used to do the behind the scenes work, with
File::Find::Rule doing the Heavy Lifting.

%prep
%setup -q -n File-Find-Rule-Age-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;

%{_fixperms} $RPM_BUILD_ROOT

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Tue Jul 28 2015 Tim Orling <ticotimo@gmail.com> - 0.302-3
- Additional cleanup per review (#1242724)

* Mon Jul 27 2015 Tim Orling <ticotimo@gmail.com> - 0.302-2
- Add dependencies from META.json
- Cleanup spec file per review (#1242724)

* Thu Apr 02 2015 Tim Orling <ticotimo@gmail.com> - 0.302-1
- Specfile autogenerated by cpanspec 1.78.
